//
//  HomeTVCell.swift
//  BottleNeckTest
//
//  Created by Arun Kumar on 1/13/17.
//  Copyright © 2017 Arun Kumar. All rights reserved.
//

import UIKit

class HomeTVCell: UITableViewCell {

    @IBOutlet var nameLabel:UILabel?
    @IBOutlet var timeLabel:UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
