//
//  HomeVC.swift
//  BottleNeckTest
//
//  Created by Arun Kumar on 1/13/17.
//  Copyright © 2017 Arun Kumar. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet var homeTableView:UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK: ButtonActions
    
    @IBAction func deleteButtonAction(_sender:UIButton){
        let button = _sender as UIButton
        
        var tagValue :NSInteger?
        tagValue = button.tag / 1001 ;
        
        if _sender.tag > 0 {
            print("tagvalue: \(tagValue)")
        }
        else{
            
        }
        
    }
    
    //MARK: TableViewCell Extension
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTVCell", for: indexPath) as! HomeTVCell
        
    
        cell.nameLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        
        let date = Date()
        _ = Calendar.current
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss"
        let DateInFormat = dateFormatter.string(from: date)
        
        cell.timeLabel?.text = DateInFormat
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        let cellStyleForEditing: UITableViewCellEditingStyle = .delete
        return cellStyleForEditing
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // tableView.deselectRow(at: indexPath, animated: true)
        
        
    }


}
